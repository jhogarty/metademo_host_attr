# README


## SUMMARY

This repo is part of a collection of related repos with the prefix of metademo\_
The purpose of this repo, and the related ones, is to assist me with the idea of
prototyping a demo application based off of an existing producution application 
which uses Sinatra/Ruby-Grape, and convert that appliation to one that uses Ruby
 on Rails, splitting out resources into their own micro-service (API).

Currently the list of related repos are:

  - [MetaDemo Host](https://gitlab.com/jhogarty/metademo_host)
  - [MetaDemo Host Attr](https://gitlab.com/jhogarty/metademo_host_attr)
  - [MetaDemo Host Iface](https://gitlab.com/jhogarty/metademo_host_iface)
  - [MetaDemo Host Mount](https://gitlab.com/jhogarty/metademo_host_mount)
  - [MetaDemo Host Role](https://gitlab.com/jhogarty/metademo_host_role)
  - [MetaDemo Host Type](https://gitlab.com/jhogarty/metademo_host_type)
  - [MetaDemo Datacenter](https://gitlab.com/jhogarty/metademo_datacenter)


## SPECS

For each resource of the application, we will identify the attributes/columns
needed in the database.  Each attribute should have a defined type.  For example:

  - hostname      => String
  - memory        => Number
  - updated\_dttm => DateTime

We also need to identify which attributes/columns are *required*.  We should 
ensure that we have the proper validations in place for these columns as well
as defining validations for say FQDN (fully qualified domain names.)

Beyond the model inspection we also need to identify what the expected endpoints
are for the application, with related *actions*.  For example, for the host 
resource we expect to be able to use GET, POST, PUT, (PATCH), and DELETE.  For
those endpoints we expect the response to be in JSON.  We need to identify what
that reponse will look like for both a single record and a collection of records.

    ```ruby
      primary_key :id
      foreign_key :host_id, :hosts
      String :name
      String :value
      String :created_by
      DateTime :created_dttm
      String :updated_by
      DateTime :updated_dttm

      # NOTE: we are replacing the rails 'timestamps' with our own columns so
      #       that we match with the pre-existing database structure.
      #
      # REQUIRED: all columns are required
    ```

**REQUIRED ENDPOINTS**

  - GET    /ap1/v1/hosts/:id/attrs      # index
  - POST   /api/v1/hosts/:id/attrs      # create
  - GET    /ap1/v1/hosts/:id/attrs/:id  # show
  - PUT    /api/v1/hosts/:id/attrs/:id  # update
  - PATCH  /api/v1/hosts/:id/attrs/:id  # update
  - DELETE /api/v1/hosts/:id/attrs/:id  # delete


